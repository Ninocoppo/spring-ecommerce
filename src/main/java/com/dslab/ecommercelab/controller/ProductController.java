package com.dslab.ecommercelab.controller;


import com.dslab.ecommercelab.entity.Product;
import com.dslab.ecommercelab.entity.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "product")
public class ProductController {

    @Autowired
    ProductRepository repository;

    @PostMapping(path = "/add")
    public @ResponseBody
    Product add (@RequestBody Product prodotto){
        return repository.save(prodotto);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Product> getAll(){
        return repository.findAll();
    }

    @GetMapping(path="/{id}")
    public @ResponseBody
    Optional<Product> getById(@PathVariable Integer id){
        Optional<Product> byId=repository.findById(id);
        return byId;
    }

}
