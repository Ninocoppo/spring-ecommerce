package com.dslab.ecommercelab.controller;


import com.dslab.ecommercelab.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping(path = "ordine")
public class OrderController {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    FinalOrderRepository finalOrderRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderProductRepository orderProductRepository;

    @GetMapping(path="/all")
    public @ResponseBody
    Iterable<FinalOrder> getOrdini(){
        return finalOrderRepository.findAll();
    }

    @PostMapping(path="/insert")
    public @ResponseBody String insertOrder(@RequestBody OrderRequest orderRequest) {
        Optional<User> utente = userRepository.findById(orderRequest.getUserId());
        if (utente == null) {

            return ("Non è possibile associare ordini a questo utente poichè non è presente nel database");

        } else {

            Set<Integer> keys = orderRequest.getProduct().keySet();
            int quantityReq;
            int id_prod;
            FinalOrder finalOrder = new FinalOrder();
            for (Integer key : keys) {
                //System.out.println(key);
                id_prod = key;
                //System.out.println(id_prod);
                Optional<Product> myProduct = productRepository.findById(id_prod);
                quantityReq = orderRequest.getProduct().get(id_prod);

                if (quantityReq <= myProduct.get().getItems()) {

                    OrderProduct lineItem = new OrderProduct();

                    lineItem.setProduct(myProduct.get());
                    lineItem.setQuantita(quantityReq);
                    myProduct.get().setItems(myProduct.get().getItems() - quantityReq);
                    finalOrder.getLista().add(lineItem);
                    //System.out.println(finalOrder.getLista().get(0).getId());
                    orderProductRepository.save(lineItem);

                    productRepository.save(myProduct.get());

                }else{

                    System.out.println ("Quantità non sufficiente del prodotto con id: "+myProduct.get().getId());
                }
            }
            System.out.println(finalOrder.getLista());
            finalOrder.setUser(utente.get());
            finalOrderRepository.save(finalOrder);
            return "Ordine inserito!!!";

        }

    }
}
