package com.dslab.ecommercelab.entity;

import org.springframework.data.repository.CrudRepository;

public interface OrderProductRepository extends CrudRepository<OrderProduct,Integer> {
}
