package com.dslab.ecommercelab.entity;

import java.util.Map;

public class OrderRequest {

    private Integer userId;

    private Map<Integer,Integer> product;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Map<Integer, Integer> getProduct() {

        return product;
    }

    public void setProduct(Map<Integer, Integer> product) {

        this.product = product;
    }
}

