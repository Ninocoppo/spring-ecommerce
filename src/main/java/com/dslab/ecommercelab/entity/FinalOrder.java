package com.dslab.ecommercelab.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class FinalOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    private User user;

    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderProduct> lista;

    public FinalOrder() {
        this.lista = new ArrayList<OrderProduct>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<OrderProduct> getLista() {
        return lista;
    }

    public void setLista(List<OrderProduct> lista) {
        this.lista = lista;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    private String stato;

    public double getTotal(){
        double total = 0.0;
        for(OrderProduct prodotto : lista){
            total+=prodotto.getTotal();
        }
        return total;
    }
}